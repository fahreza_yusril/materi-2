const ul = document.querySelector('.items');

ul.firstElementChild.textContent = 'Batman';
ul.children[1].innerText = 'Who Laughs';
ul.lastElementChild.innerHTML = '<h1>The Darkest Knight</h1>';

const btn = document.querySelector('.btn');
btn.style.background = 'red';
