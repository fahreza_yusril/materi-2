const nama = 'Fahreza Yusril Mahendra';
const umur = 24;

console.log('Hello, I am ' + nama + ' and now, I am ' + umur);
console.log(`Nama saya ${nama} dan saya berusia ${umur} tahun`);

const a = 'I love you in every universe';
console.log(a.substring(0, 6).toUpperCase());

console.log(a.toLowerCase());
console.log(a.split(''));

const semesta = 'earth0, earth-22, earth69, earth666';
console.log(semesta.split(', '));
