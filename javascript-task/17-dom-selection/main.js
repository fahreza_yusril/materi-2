// Single Element
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));

// Multiple Elements
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));
