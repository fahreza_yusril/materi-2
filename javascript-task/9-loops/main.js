const todos = [
  {
    id: 1,
    text: 'Berangkat Magang',
    isComplete: true,
  },
  {
    id: 2,
    text: 'Mengerjakan Tugas',
    isComplete: false,
  },
  {
    id: 3,
    text: 'Tidur',
    isComplete: true,
  },
];

for (let i = 0; i <= 10; i++) {
  console.log(`For Loop Number: ${i}`);
}

let i = 0;
while (i <= 10) {
  console.log(`While Loop Number: ${i}`);
  i++;
}

for (let i = 0; i < todos.length; i++) {
  console.log(` Todo ${i + 1}: ${todos[i].text}`);
}

for (let todo of todos) {
  console.log(todo.text);
}
