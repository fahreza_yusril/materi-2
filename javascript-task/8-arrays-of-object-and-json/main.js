const todos = [
  {
    id: 1,
    text: 'Berangkat Magang',
    isComplete: true,
  },
  {
    id: 2,
    text: 'Mengerjakan Tugas',
    isComplete: false,
  },
  {
    id: 3,
    text: 'Tidur',
    isComplete: true,
  },
];

console.log(todos);
console.log(todos[1].text);
