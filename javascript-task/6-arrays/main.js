const numbers = [1, 2, 3, 4, 5];
const superhero = ['batman', 'spiderman', 'ironman', 'flash'];
console.log(numbers);
console.log(numbers[1]);

superhero[4] = 'superman';
console.log(superhero);

superhero.push('deathstroke');
superhero.unshift('wolverine');
superhero.pop();

console.log(Array.isArray(superhero));
console.log(superhero.indexOf('batman'));
