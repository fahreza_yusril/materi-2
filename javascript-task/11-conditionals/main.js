const t = 45;
const u = 23;

if (t == 10) {
  console.log('t is 45');
} else if (t > 10) {
  console.log('t is greater than 10');
} else {
  console.log('t is less than 10');
}

if (t > 25 || u > 10) {
  console.log('t is more than 25 or u is more than 10');
}

color = 'blue';
switch (color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:
    console.log('color is not red or blue');
}

const c = color === 'red' ? 10 : 20;
console.log(c);
