const todos = [
  {
    id: 1,
    text: 'Berangkat Magang',
    isComplete: true,
  },
  {
    id: 2,
    text: 'Mengerjakan Tugas',
    isComplete: false,
  },
  {
    id: 3,
    text: 'Tidur',
    isComplete: true,
  },
];

todos.forEach(function (todo, i, myTodos) {
  console.log(`${i + 1}: ${todo.text}`);
  console.log(myTodos);
});

const todoTextArray = todos.map(function (todo) {
  return todo.text;
});
console.log(todoTextArray);

const todo1 = todos.filter(function (todo) {
  return todo.id === 1;
});
