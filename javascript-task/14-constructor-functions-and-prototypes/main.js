function Person(firstName, lastName, dob) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.dob = new Date(dob);
}

Person.prototype.getBirthYear = function () {
  return this.dob.getFullYear();
};

Person.prototype.getFullName = function () {
  return `${this.firstName} ${this.lastName}`;
};

const person1 = new Person('Bruce', 'Wayne', '27-12-1980');
const person2 = new Person('Slade', 'Wilson', '5-11-1975');

console.log(person2.getFullName());
console.log(person1);
